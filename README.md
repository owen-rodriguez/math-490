# Math 490

## Workhseet 15 Problem 3,4 Write-up
[Write-up pdf](https://gitlab.com/owen-rodriguez/math-490/blob/master/worksheet_15_p3_p4.pdf)

[LaTeX source](https://gitlab.com/owen-rodriguez/math-490/blob/master/worksheet_15_p3_p4.tex)

## Worksheet 3 Problem 1 Write-up

[Write-up pdf](https://gitlab.com/owen-rodriguez/math-490/blob/master/worksheet_3_writeup.pdf)

[LaTeX source](https://gitlab.com/owen-rodriguez/math-490/blob/master/worksheet_3_writeup.tex)
